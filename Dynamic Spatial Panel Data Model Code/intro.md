MATLAB Code

Dynamic Spatial Panel Data Model Code

The following code is an adaptation of Paul Ehhorst's dynamic spatial panel data code with two additional features. First, the stability condition is calculated along with its associated 95% confidence interval. Second, the effects estimates are printed out in an easy to read format suitable for cutting and pasting into papers, presentations, etc.

The file "handbook82_Lacombe_original.m" is the same file as in the original Elhorst code but with the two additions mentioned above.
